# P.O.W.E.R.F.U.L Presentations

O'Reilly course Oct 1 2020 - Curtis Newbold


## Intro

> Communication is about getting others to adopt your point of
> view, to help them understand why your excited (or sad,
> or optimistic, or whatever else you are)
> <i> - Seth Godin</i>

An approach, not a process

Approaches are:
* Conceptual
* Flexible
* Adaptable
* Organic
* But.... Strategic

Processes are:

* Ordered
* Linear
* Rigid
* Forumlaic

Powerful presentations are:

CREATIVE
* Passionate
* Unique
* Authentic
* Imaginitive
* Designed

... but Restrained
* Strategic
* Organised
* Relevant
* Simple
* Planned
* Purposeful
* Practiced

<b>For me to work on:</b>
* Shake voice (More breath, raise voice pitch, shorter sentences)
* Speed speech ( Time yourself, deep breaths)
* Look at screen and notes
* Flood with fillers and qualifiers ( What's my filler? Replace with a pause)


## P is for Prepare

<b>Most important:</b>

 * What is my point?
 * Why would they care?

<b>Questions that matter:</b>

* Who is in the audience?
* What's their state of mind?
* What will they expect?
* How much do they know?
* How much do they need?
* How much time do I have?
* What will the room look like?

<b>Questions that distract:</b>
* What pictures?
* How many slides?
* How many bullets?
* What should I wear?

<b>Type of Purpose:</b>
* Informative
* Demonstrative
* Persuasive
* Motivational

<b>Pick your type</b>
* Scripted
 - (+) Planned, timed, ordering , structure
 - (-) Engage, energy
* Memorized
 - (+) Shows preparation, strong call to action, eye contact
 - (-) High prep, freezing, could be nervous
* Extemporaneous
 - (+) Adaptable, engaging, more natural
 - (-) Rehearsing, strong knowledge of content
* Impromptu
 - (+) Engaging, no preparation
 - (-) Off track, Timing, losing key messages, need content knowledge

<b>Plan: Assemble Ideas</b>
 * Passion, Explore ideas, Artist-Designer mindset
 * Ditch the sofware
   - Post it's, whiteboards, pencils - mind maps
   - Brainstorm with peers

<b>Adapt</b>
 * Speak on their level, meet their needs, respect their time
 * Know the location, project your voice, 2 diff computers




## O is for Open

<b> Introduce yourself, your topic &#128515;</B>

 * Thank your audience
 * Name, Role, Affiliation
 * Label your topic
 * Purpose of presentation (Why they should care)
 * If unclear, state how long presentation will be

<b> Grab Attention &#128526;</b>

 * Story or Anecdote
 * Impressive statistics, Provocative statements
 * Relevant joke, imaginitive scenario
 * Demo
 * Quote, ask a question -> Segue into topic

Why:  First minute will decide if grab attention, make topic relevant, timely and meaningful, improve recall

<b>Orient</b>

## W is for Weave

A story from past or findings on the research:

Stories - 5 components
Character
settings
Plot
Conflict
Resolution

Three parts:

<b>Act 1:</b> Set up, background, characters, relationships, unfulfilled desire

<b>Act 2:</b> The conflict, obstacles or issues prevent character from reaching desired state

<b>Act 3:</b> Resolve conflict, new solution, new learning, solution, ending or result

<img src="./Story.png"/>

7 - 10 minutes is sweet spot for referencing or adding new stories

Shorter presentation:  One story, revisit at end - possibly mention throughout

Story + Pictures - 65% better recall

## E is for Express

Pictures & Graphics...
Visuals over bulletpoints

Types of slides - only 3,4,5

White / Black/Dark & Accent - High contrast

<img src="./SlideTypes.png" />

### Improving Visuals

Photos, icons instead of Bullets?
Bar charts - colours represented by Icons as a legend

## R is for Relate

Relate with delivery, body language

Pre-Communication message:
Create clear precise title
Expectations - then deliver
Use tone setting, revealing visuals

Meet expectations or pleasantly surprise, Don't confuse or offend;

Eye contact, Emotion - be passionate, don't be a robot

Body Language -  Confidence, Poise, Connection, Passion, Interest

Move - accentuate , augment message & increase connection

Powerpoint: Mute Screen 'B' key in powerpoint

## F is for Frame

Creating organisational structure

### Beginning
 Intro
 Attention Grabber
 Roadmap

### Middle
 Organised content
 Signposts
 Transitions

### End
  Summary
  Conclusion
  Call-to-Action

<img src="Transitions.png"/>

## U is for Unify

Summarise & Connect

 * Primary messages
 * Come full circle
 * Leave nothing unanswered, close stories, research, findings

## L is for Leave

### Create the Aha..

Make it complete
Make it all make sense
Make it memorable
Make it stick

(Make sure there is a conclusion)

### What is the call to Action..

For your audience types

<img src="./ActionWords.png" />

## Overview

## References

Curtis Newbold

[Seth Godin - (Really bad Powerpoint](https://seths.blog/2007/01/really_bad_powe/)

[Nancy Duarte - secret structure of great talks](https://www.ted.com/talks/nancy_duarte_the_secret_structure_of_great_talks?referrer=playlist-how_to_make_a_great_presentation)

[Curtis Newbold - 60 minutes to designing a better powerpoint slide](https://learning.oreilly.com/live-training/courses/60-minutes-to-designing-a-better-powerpoint-slide/0636920284079/)

Fake it till you become it - Amy Cuddy
https://blog.ted.com/fake-it-til-you-become-it-amy-cuddys-power-poses-visualized/

Nancy Duarte - Slide:Ology


https://www.duarte.com/presentation-skills-resources/how-to-write-a-call-to-action-in-a-persuasive-speech/

https://www.duarte.com/presentation-skills-resources/category/tips/
