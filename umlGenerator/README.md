# UML Generator

This section gives a mechanism for using PlantUML syntax to generate
UML diagrams. These can be used in design documents or presentations.

## Pre-Requisites

The following software is required to automate the generation of multiple plantUML diagrams.

* Java JDK 1.6+
* Maven 3.2+

## Reference - How to write PlantUML syntax

[PlantUML Reference site](http://plantuml.com/)

### Changing PlantUML from default style

The `skinparam` command in plantUML allows changes to the drawing style and are detailed in the [PlantUML Skinparam reference](http://plantuml.com/skinparam.html)

Some of the most promising to consider may be the 'canned' versions of the following:

* `skinparam monochrome true` - generates monochrome diagrams
* `skinparam handwritten true` - generates handwritten style diagrams


## Online PlantUML Editor

Below are some online plantUML editors that can be used for fast feedback editing.

[PlantText](http://www.planttext.com/planttext)

[PlantUML Server](http://plantuml.com/plantuml/)

## PlantUML Maven plugin

[Github - PlantUML Maven plugin](https://github.com/jeluard/maven-plantuml-plugin)

This is no longer maintained  :disappointed:

### Other options

A plantuml.jar is available and there are other options to generate diagrams outside of the maven plugin.

[Running PlantUML](http://plantuml.com/running.html)


# How to use it

## Adding a new diagram

Create a new file under `src\plantUML` with the suffix `.plantuml`
Some samples are provided.

## How to run the generation

```
>mvn clean com.github.jeluard:plantuml-maven-plugin:generate
```