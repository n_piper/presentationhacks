# Presentation Template Toolkit

The presentation is a sample built on advice from "The McKinsey Edge: Success principles from the World's most powerful Consulting Firm"

## Guidelines

1. What are the background, issues, and objectives?

2. What changes are we bringing?

3. What are the impacts to be quantified?

4. What are the key questions to be answered?

5. What are the key workshops and meetings within the detailed work plan?

6. What is the proposed project team setup?

## Background , Issues & Objectives

Create a “background, issues, and objectives” template to give context to whatever you do. Whether it is to explain the current situation, current complications, or a short background history, it is important to start your presentation with a meaningful context.

Past on the Left,.. Future on the right.

Forcing yourself to convey your message in a few bullet points requires practice. This synthesizing effort has a large payoff, though. Listeners can’t go beyond three or four large-font bullet points or they lose interest.

from an audience point of view: it’s usually just, “Tell me the big problem and what you are going to do,”

## CHANGES WE BRING

Having a “changes we bring” template works especially well in transformation projects. We call this the “from” and “to” model. (AS-IS, TO-BE)

The template has “from” and “to” columns that show the current state and the desired future state. It’s a quicker way to show the end state: where do we want to finish? At the leadership or board level, swiftness and agility count. It’s also a precise way to synthesize all of your learning (if you carried out interviews, field research, analysis, etc.) into five or six bullet points. Drafting an ideal state, the Emerald City, makes sense for anyone. Listeners appreciate this approach because it gives a big picture.

But something easy to understand may be difficult to create... the shorter the presentation, ironically prep time gets harder.

## NUMBERS TELL THE STORY

As consultants we speak in numbers. If it’s a productivity enhancement, we speak in percentages. If it’s fixing the inventories and store sales, we speak in turnover ratios. Numbers tell the truths that words may lie about. Without numbers, it’s difficult to objectively assess where to play, where to win, and how to win—in a strategy playbook, for example. Numbers are the backbone of any presentation and should be included with an end output image early on. 

Using a “numbers tell the story” template, you are going to tell the audience, “These numbers are what you are going to find out.” Force yourself to “dummy” out a couple of impact calculation slides. 

Aligning this end state with other senior leaders early on can prevent you from chasing after the wrong goal.


## KEY QUESTIONS TO ANSWER

The key five to six questions on a “key questions to answer” template should structure your overall document. These questions should clearly link back to your objectives (shown on the first BIO template), so you need to have consistency. 

Let’s say one of your objectives was “to come up with the right operating model.” Then in this question section, you would need to address this, for example by saying, “What are the potential combinations of operating models to fit your future growth plans?” or “What are some competitor benchmark references?” 

Some people have asked me what ties a presentation together in a lucid way. The short answer, I’ve discovered, is the main questions and subquestions. That’s because framing a question requires a deep mental effort—it’s challenging.

Consider the subtle but major effect that framing a question can have on you and your audience. 

For example, let’s examine the following three questions:

* Do you want your company’s decision-making body to be more in control or less?

* Do you want your company to give more power to subsidiaries?

* Which operating model, a centralized model or decentralized model, do you want to establish?



## MASTER WORK PLAN

Gantt charts, a bar-type project scheduling format developed by Henry Gantt in the early 1900s, were nonexistent in my work before joining McKinsey. I thought visualizing a work plan was a waste of time. But I was wrong. There are things you can’t see in a column- and-row-based Excel sheet.

The “master work plan” template is like a Gantt chart, just in more detail. What’s more, it shows you the critical path to project completion. By definition, critical path means the least amount of output or the most efficient means required to get to your final destination. A critical path is necessary when making anything because it allows you, by completing necessary prerequisites, to transition from one stage to another.

## PEOPLE SETUP

Projects have stakeholders. Organizing responsibility and roles up front using a “people setup” template will align the expectations of these individuals. The ingredients that make up a good people setup are a well-defined hierarchy of participants both external and internal to your particular project, role and duty expectations, and governance policy or KPIs. After the team structure is completed you need to align the output with individuals, forcing you to remove any unnecessary people on the team or add people if needed. 
