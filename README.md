# Presentation Tech

What this is -- a selection of media, templates links & content for my
professional presentations.

It is mainly using the 'remark' template but looks to extend it for use in
presentations.

I can't draw and hive a relatively basic level of artistic talent so I borrow the philosophy in this post (forwarded me by Rajiv Kilparti)

[Developing Solutions - "Always look for ways to cheat"](http://0xdabbad00.com/2015/05/31/developing_solutions/)

## Reveal

Looks better & more feature rich than remark - trialling a version.
Allows for PDF Printing, Presenter notes, full bleed images.

A trial is available under `samples\reveal\test.html`
I've also created a new brand based css sheet.

Also quite a few extensions / plugins like voiceovers etc; to play with.

[Github Reveal.js](https://github.com/hakimel/reveal.js)

## Remark

Remark/Javascript is a template for creating presentations via HTML using simple markdown for the slide transitions.

A skeleton is available at `indexSkeleton.html`

[Github - Remark](https://github.com/gnab/remark/wiki)

[How to - Using Remark](http://www.lendmeyourear.net/wp-content/uploads/markdown-remark-slides.html#1)

[An example presentation](http://www.markdowner.com/document/511b9fea4913db020000000e#1)


## Markdown

Remark allows use of the Markdown (.md) format as a slide content. Here's some tips on how to write basic remark style.

[Markdown Syntax](http://daringfireball.net/projects/markdown/)

[Markdown/Remark for presentations](https://github.com/gnab/remark/wiki/Markdown)

[Emoticons for your markdown](http://www.emoji-cheat-sheet.com/)

# HTML References

[W3C HTML 5](http://www.w3schools.com/html/default.asp)
[W3C Javascript](http://www.w3schools.com/js/default.asp)

## CSS

[W3C CSS Reference](http://www.w3schools.com/cssref/)


## Colours

The excellent [colorhexa](http://www.colorhexa.com/) is a useful guide for picking color swatches, tones.

A [Colour Wheel](https://www.sessions.edu/color-calculator/) is a useful way to pick complementary

## Fonts

There are some excellent fonts for presentation. The current .css files under `media` utilise some of these web fonts in the stylesheets.

[Google Web fonts](https://www.google.com/fonts)

[How to use google web fonts](https://developers.google.com/fonts/docs/getting_started)

## Icons 

http://www.flaticon.com/
https://thenounproject.com/
https://icons8.com/

### Social Media

There are icons for github & linkedIn under the `media` folder sourced from their websites for use on end of presentation pages.

Currently using some on the standard 'end' page.

### Font-Awesome for technology icons & social media

The library [FontAwesome](http://fortawesome.github.io/Font-Awesome/icons/) has over 600 icon, brand, technology & social media options - include like a google web font. - [How to include](http://fortawesome.github.io/Font-Awesome/get-started/)

[W3C Icons Reference](http://www.w3schools.com/icons/default.asp)

## Content & written word

Your presentation or site is only as effective as your communication.

[Hemmingway](http://www.hemingwayapp.com/) is an app to improve the clarity of your copy and content.

## Openstock Image content

The following are royalty free images.

[Wikimedia commons](https://commons.wikimedia.org/wiki/Category:Images#)

[Search creative commons](http://search.creativecommons.org/)

[Flickr](https://www.flickr.com/)

[Vector Stock](https://www.vectorstock.com)

[pixabay.com](http://pixabay.com)

[unsplash.com](http://unsplash.com)

[wikimediacommons.com](http://wikimediacommons.com)

[publicdomainpictures.net](http://publicdomainpictures.net)

[thenounproject.org](http://thenounproject.org)

[flaticon.com](http://flaticon.com)

[freepik.com](http://freepik.com)



## Charts & Graphics

HTML Versions of charts are also available, often using Vector graphics.

### Highcharts - Javascript charts (Commercial license needed)

[Highcharts](http://www.highcharts.com)

### Google Charts

https://developers.google.com/chart/interactive/docs/

There is a sample under `./samples/charts`


## SVG

SVG is a more scalable friendly format, for graphics that has smoother lines and scales better on different devices.

### How to put SVG in web pages
http://www.sitepoint.com/add-svg-to-web-page/

### Janvas - online SVG editor
http://www.janvas.com/site/home_en.php

### Gliffy - for vector graphics
https://www.gliffy.com/go/html5/launch?app=1b5094b0-6042-11e2-bcfd-0800200c9a66

### Snap SVG - SVG Javascript library
http://snapsvg.io/

## UML Diagrams

Under `umlGenerator` is a maven based method to bulk generate UML diagrams using PlantUML.

## Comics

Comics are becoming a fast way of communicating ideas & storyboards. Without drawing ability.. a potential way to generate these online.

Free comics
http://mashable.com/2010/10/24/create-your-own-comics/#ZtFjMAJ7IPqt

http://stripgenerator.com/strip/create/

https://www.pixton.com/for-fun

# Recording your presentation

Quicktime - record .ogg files for future playback


IceCream Screen Recorder
http://icecreamapps.com/Screen-Recorder/

Powerpoint - turning into a video
https://support.office.com/en-us/article/Turn-your-presentation-into-a-video-c140551f-cb37-4818-b5d4-3e30815c3e83

Quicktime - a how to on recording a presentation for Mac

http://etc.usf.edu/techease/4all/getting-started/creating-screen-recordings-with-quicktime-player/


# Visual & Artistic services

Sometimes for the extra effect it's worth paying a professional - some options:

[color.adobe.com](https://color.adobe.com)

## Where to host? Options?
HTML based presentations can be made accessible from a publicly accessible static file server. 

Reveal.js also gives the option of using this type of setup to scroll through people who have connected!

Examples include: GitHub Pages, Amazon S3, Dreamhost, Akamai, Heroku

Github pages - https://pages.github.com/

## FiveRR 

An artistic / freelance site for cartoons, portraits, writing copy, whiteboard explainer videos etc;

[Whiteboard Explainers](https://www.fiverr.com/categories/video-animation/whiteboard-explainer-videos)

[Presentations](https://www.fiverr.com/categories/business/online-presentations/#layout=auto&page=1)

[Video Intros](https://www.fiverr.com/categories/video-animation/custom-video-intros/#layout=auto&page=1)

[Voice overs](https://www.fiverr.com/categories/music-audio/voice-overs/#layout=auto&page=1)

## Full Bleed image CSS

Replicating the full bleed image look in HTML presentations.

[https://css-tricks.com/perfect-full-page-background-image/](https://css-tricks.com/perfect-full-page-background-image/)
